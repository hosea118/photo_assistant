import face_recognition
import logging as logger
import os
import photo_exif
import cv2
import numpy as np
import face_recognition_knn as fc_knn

#空列表
#known_faces = []

def resize_image(image):
    img_h, img_w = image.shape[:2]
    if img_w > 1800 or img_h > 1600:
        image = cv2.resize(image, (int(img_h/3), int(img_w/3)))
    elif img_w == img_h == 0:
        image = cv2.resize(imgae, (int(img_h/3), int(img_w/3)))
    return image

def detect_faces_in_image_use_opencv(image, default_h=300, default_w=300):
    #print('loading model...')
    net = cv2.dnn.readNetFromCaffe('photo_assistant/deploy.prototxt.txt', 'photo_assistant/res10_300x300_ssd_iter_140000.caffemodel')
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (default_h,default_w)), 1.0, (default_h,default_w), (104.0, 177.0, 123.0))
    #print('computing face detections...')
    net.setInput(blob)
    detections = net.forward()
    face_bounding_boxes = []
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.5:
            box = detections[0,0,i,3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype('int')
            if h < startY or h < endY or w < startX or w < endX:
                logger.info('error face bounding!!! (w:%d, h:%d)(%d, %d, %d, %d) %2f' % (w,h,startX, startY, endX, endY, confidence))
            else:
                logger.info('find a face, (%d, %d, %d, %d) %2f' % (startX, startY, endX, endY, confidence))
                face_bounding_boxes.append((startY, startX, endY, endX))
    return face_bounding_boxes

def detect_faces_in_image_use_face_recognition(image):
    image_after_resize = resize_image(image)
    face_bounding_boxes = face_recognition.face_locations(image_after_resize)
    return face_bounding_boxes

def face_recognition_in_pic(file_name, known_faces_encodeing, model_path):
    if not os.path.exists(file_name):
        logger.error('%s not exists' % file_name)

    try:
        unknown_image = cv2.imread(file_name)
    except OSError:
        logger.error('load_image_file error')
        return 'error_img'

    if unknown_image is None:
        return 'error_img'

    logger.info("handle img " + file_name)
    logger.info('step 1, use knn model to locate faces and recognize it')
    #use knn model first
    predictions = fc_knn.predict(file_name, model_path=model_path)
    faces = len(predictions)
    if faces > 3:
        return 'more_faces'
    elif faces == 3:
        return 'three_faces'
    elif faces == 2:
        return 'two_faces'
    elif faces == 0:
        return 'no_face'
    elif faces == 1 and predictions[0][0] != 'unknown':
        return predictions[0][0]

    logger.info('step 2, find a unknown face, classify it')
    #use face_compare to classify
    image_rgb = cv2.cvtColor(unknown_image, cv2.COLOR_BGR2RGB)
    face_bounding_boxes = detect_faces_in_image_use_opencv(unknown_image)

    faces = len(face_bounding_boxes)
    if faces > 3:
        return 'more_faces'
    elif faces == 3:
        return 'three_faces'
    elif faces == 2:
        return 'two_faces'
    elif faces  == 0:
        return 'no_face'
    else:
        unknown_face_encoding = face_recognition.face_encodings(image_rgb, known_face_locations=face_bounding_boxes, num_jitters=100)[0]
    for key in known_faces_encodeing.keys():
        results = face_recognition.compare_faces(known_faces_encodeing[key], unknown_face_encoding, tolerance=0.30)
        if True in results:
            logger.info('find a known face: '+ key)
            return key
            
    #print('unknown face, add to known_faces_encodeing')
    new_key = 'person' + str(len(known_faces_encodeing)-1)
    unknown_face_list = []
    unknown_face_list.append(unknown_face_encoding)
    known_faces_encodeing[new_key] = unknown_face_list 
    return new_key
